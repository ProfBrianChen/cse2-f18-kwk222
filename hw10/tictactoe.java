// Kenny Kwock / kwk222@lehigh.edu
// Homework #10, 11/22/2018
// Creating tic-tac-toe in a terminal

import java.util.Scanner;

public class tictactoe {

    public static void main(String[] args) {
        String[][] board = {{"1","2","3"},{"4","5","6",},{"7","8","9"}};

        print(board);

        int position;
        int win = 0; //0 is tie, 1 means 1 won, 2 means 2 won
        String player1 = "x";
        String player2 = "o";
        Scanner scnr = new Scanner(System.in);
        boolean tempBool = false;
        game:for(int i = 1; i <= 9; i++) {//The game only goes on for 9 turns
            int parity = i % 2;//This line determines the turn, and who wins the game
            if(parity == 1) {
                System.out.println("Player one, please enter the desired position to place an \"x\": ");
                do {
                    position = integerChecker(scnr);
                    tempBool = overWrite(position, board);
                } while(tempBool);
              System.out.println();
                input(position, player1, board);
            }
            else {
                System.out.println("Player two, please enter the desired position to place an \"o\": ");
                do {
                    position = integerChecker(scnr);
                    tempBool = overWrite(position, board);
                } while(tempBool);
              System.out.println();
                input(position, player2, board);
            }

            print(board);//Prints and checks the board after each input
            win = winStatus(board, parity);

            if(win > 0) {//This line keeps detecting whether the game has a win or not
                break game;
            }

        }

        if(win == 0) {//Towards the end of main method, determines who won. If nobody wins, its a tie
            System.out.println("The game has ended in a tie!");
        }
        else if(win == 1) {
            System.out.println("Player 1 has won the game!");
        }
        else {
            System.out.println("Player 2 has won the game!");
        }

    }

    public static String[][] input(int position, String mark, String[][] board) {//Method that determines position/input
        switch(position) {
            case 1:
                board[0][0] = mark;
                break;
            case 2:
                board[0][1] = mark;
                break;
            case 3:
                board[0][2] = mark;
                break;
            case 4:
                board[1][0] = mark;
                break;
            case 5:
                board[1][1] = mark;
                break;
            case 6:
                board[1][2] = mark;
                break;
            case 7:
                board[2][0] = mark;
                break;
            case 8:
                board[2][1] = mark;
                break;
            case 9:
                board[2][2] = mark;
                break;
        }
        return board;
    }

    public static boolean overWrite(int position, String[][] board) {//Method that examines for an overwrite
        boolean tempBool = false;
        switch(position) {
            case 1:
                if(board[0][0].equals("x") || board[0][0].equals("o")) {
                    System.out.println("Invalid input, please try again: ");
                    tempBool = true;
                }
                break;
            case 2:
                if(board[0][1].equals("x") || board[0][1].equals("o")) {
                    System.out.println("Invalid input, please try again: ");
                    tempBool = true;
                }
                break;
            case 3:
                if(board[0][2].equals("x") || board[0][2].equals("o")) {
                    System.out.println("Invalid input, please try again: ");
                    tempBool = true;
                }
                break;
            case 4:
                if(board[1][0].equals("x") || board[1][0].equals("o")) {
                    System.out.println("Invalid input, please try again: ");
                    tempBool = true;
                }
                break;
            case 5:
                if(board[1][1].equals("x") || board[1][1].equals("o")) {
                    System.out.println("Invalid input, please try again: ");
                    tempBool = true;
                }
                break;
            case 6:
                if(board[1][2].equals("x") || board[1][2].equals("o")) {
                    System.out.println("Invalid input, please try again: ");
                    tempBool = true;
                }
                break;
            case 7:
                if(board[2][0].equals("x") || board[2][0].equals("o")) {
                    System.out.println("Invalid input, please try again: ");
                    tempBool = true;
                }
                break;
            case 8:
                if(board[2][1].equals("x") || board[2][1].equals("o")) {
                    System.out.println("Invalid input, please try again: ");
                    tempBool = true;
                }
                break;
            case 9:
                if(board[2][2].equals("x") || board[2][2].equals("o")) {
                    System.out.println("Invalid input, please try again: ");
                    tempBool = true;
                }
                break;
        }
        return tempBool;
    }

    public static int winStatus(String[][] board, int parity) {//Chunk of conditions that determines a win or not
        if( ( (board[0][0].equals(board[0][1])) && (board[0][1].equals(board[0][2])) && (board[0][2].equals(board[0][0])) ) ||
                ( (board[1][0].equals(board[1][1])) && (board[1][1].equals(board[1][2])) && (board[1][2].equals(board[1][0])) ) ||
                ( (board[2][0].equals(board[2][1])) && (board[2][1].equals(board[2][2])) && (board[2][2].equals(board[2][0])) ) ||
                ( (board[0][0].equals(board[1][0])) && (board[1][0].equals(board[2][0])) && (board[2][0].equals(board[0][0])) ) ||
                ( (board[0][1].equals(board[1][1])) && (board[1][1].equals(board[2][1])) && (board[2][1].equals(board[0][1])) ) ||
                ( (board[0][2].equals(board[1][2])) && (board[1][2].equals(board[2][2])) && (board[2][2].equals(board[0][2])) ) ||
                ( (board[0][0].equals(board[1][1])) && (board[1][1].equals(board[2][2])) && (board[2][2].equals(board[0][0])) ) ||
                ( (board[0][2].equals(board[1][1])) && (board[1][1].equals(board[2][0])) && (board[2][0].equals(board[0][2])) ) ) {
            if (parity == 1) {
                return 1;
            }
            else {
                return 2;
            }
        }
        return 0;
    }

    public static void print(String[][] board) {//Standard multi-dimensional array print method
        for(int j = 0; j < 3; j++) {
            for(int k = 0; k < 3; k++) {
                System.out.print(board[j][k]);
                System.out.print("\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static int integerChecker(Scanner scnr) {//Just to make sure integers are within range and correct primitive
        boolean tempBool = false;
        int variable = 0;
        while(!tempBool) {
            tempBool = scnr.hasNextInt();
            if(tempBool == true) {
                variable = scnr.nextInt();
                if((variable > 0) && (variable < 10)) {//refer to cse2 for flip
                    break;
                }
                else {
                    System.out.println("Invalid input, please try again: ");
                    tempBool = false;
                }
            }
            else {
                System.out.println("Invalid input, please try again: ");
                scnr.next();
            }
        }
        return variable;
    }

}