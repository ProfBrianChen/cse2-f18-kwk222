import java.util.Random;
import java.util.Scanner;

public class randomSentence {

    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        Random rand = new Random(); //1 = yes, 0 == no

        System.out.println("Please input 1 for sentence, or 0 to quit");
        int input = scnr.nextInt();
        while (input == 1) {
            System.out.print(paragraph(rand));
            input = scnr.nextInt();
        }
    }

    public static String paragraph(Random rand) {
        return sentence(rand) + " " + sentence(rand) + " " + sentence(rand) + " " + sentence(rand);
    }

    public static String sentence(Random rand) {
        return "The " + adjective(rand) + " " + nounSubject(rand) + " " + verb(rand) + " the " + nounObject(rand) + ".";
    }

    public static String adjective(Random rand) {
        String adjective = "";
        int a = rand.nextInt(10);
        switch (a) {
            case 0:
                adjective = "curvy";
                break;
            case 1:
                adjective = "spotty";
                break;
            case 2:
                adjective = "huge";
                break;
            case 3:
                adjective = "small";
                break;
            case 4:
                adjective = "cool";
                break;
            case 5:
                adjective = "old";
                break;
            case 6:
                adjective = "fat";
                break;
            case 7:
                adjective = "lumpy";
                break;
            case 8:
                adjective = "bitter";
                break;
            case 9:
                adjective = "exotic";
                break;
        }
        return adjective;
    }

    public static String nounSubject(Random rand) {
        String nounSubject = "";
        int b = rand.nextInt(10);
        switch (b) {
            case 1:
                nounSubject = "elephant";
                break;
            case 2:
                nounSubject = "tiger";
                break;
            case 3:
                nounSubject = "snake";
                break;
            case 4:
                nounSubject = "eagle";
                break;
            case 5:
                nounSubject = "alligator";
                break;
            case 6:
                nounSubject = "kangaroo";
                break;
            case 7:
                nounSubject = "koala";
                break;
            case 8:
                nounSubject = "chicken";
                break;
            case 9:
                nounSubject = "dog";
                break;
        }
        return nounSubject;
    }

    public static String verb(Random rand) {
        String verb = "";
        int c = rand.nextInt(10);
        switch (c) {
            case 0:
                verb = "ate";
                break;
            case 1:
                verb = "bought";
                break;
            case 3:
                verb = "threw";
                break;
            case 4:
                verb = "shaved";
                break;
            case 5:
                verb = "owned";
                break;
            case 6:
                verb = "picked";
                break;
            case 7:
                verb = "preserved";
                break;
            case 8:
                verb = "tamed";
                break;
            case 9:
                verb = "carried";
                break;
        }
        return verb;
    }

    public static String nounObject(Random rand) {
        String nounObject = "";
        int d = rand.nextInt(10);
        switch (d) {
            case 0:
                nounObject = "potato";
                break;
            case 1:
                nounObject = "tomato";
                break;
            case 2:
                nounObject = "celery";
                break;
            case 3:
                nounObject = "carrot";
                break;
            case 4:
                nounObject = "apple";
                break;
            case 5:
                nounObject = "orange";
                break;
            case 6:
                nounObject = "banana";
                break;
            case 7:
                nounObject = "egg";
                break;
            case 8:
                nounObject = "peanut";
                break;
            case 9:
                nounObject = "mango";
                break;
        }
        return nounObject;
    }


}
