// Kenny Kwock / kwk222@lehigh.edu
// Homework #7, 10/29/2018
// This program will analyze text using a variety of methods and string analysis code

import java.util.Scanner;

public class textAnalyzer {

    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);

        String finalText = sampleText(scnr);//Method that stores the text into its finalText form
        char input = printMenu(scnr);//This is to store the character the user types in
        switch (input) {
            case 99:
                System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(finalText) + ".");
                break;
            case 119://w
                System.out.println("Number of words: " + getNumOfWords(finalText) + ".");
                break;
            case 102://f
                System.out.println("Number of times requested text appeared: " + findText(finalText));
                break;
            case 114://r
                System.out.println("Edited text: " + replaceExclamation(finalText));
                break;
            case 115://s
                System.out.println("Edited text: " + shortenSpace(finalText));
                break;
        }
    }

    public static String sampleText(Scanner scnr) {
        System.out.println("Please input a sample text, hit enter when you are done!");
        String text = scnr.nextLine();
        return text;
    }

    public static char printMenu(Scanner scnr) {//Menu Printing
        System.out.println("MENU");
        System.out.println("c - Number of non-whitespace characters");
        System.out.println("w - Number of words");
        System.out.println("f - Find text");
        System.out.println("r - Replace all exclamation marks with periods");
        System.out.println("s - Shorten spaces");
        System.out.println("q - quit");
        System.out.println();
        System.out.println("Please select an option: ");

        char option = scnr.next(".").charAt(0);
        while((option != 99) && (option != 119) && (option != 102) && (option != 114) && (option != 115) && (option != 113)) {
            System.out.println("Please input a valid option");
            option = scnr.next(".").charAt(0);
        }
        if (option == 113) {
            System.exit(0);
        }
        return option;
    }

    public static int getNumOfNonWSCharacters(String text) {//Counts the number of charcters, not including White Space
        int textLength = text.length();
        int characterCount = 0;
        for (int i = 0; i < textLength; i++) {
            if (text.charAt(i) != ' ') {
                characterCount++;
            }
        }
        return characterCount;
    }

    public static int getNumOfWords(String text) {//Counts the number of words, using the spaces inbetween
        int textLength = text.length();
        int wordCount = 1;
        for (int i = 0; i < textLength; i++) {
            if (text.charAt(i) == ' ') {
                wordCount++;
            }
        }
        return wordCount;
    }

    public static int findText(String text) {//This is to search for the # of times certain word appears
        Scanner scnr = new Scanner(System.in);
        System.out.println("Please input text to be found: ");
        String search = scnr.nextLine();
        int repeatWord = 0;
            while (text.contains(search)) {
                repeatWord++;
                text = text.substring(text.indexOf(search) + search.length());
            }
        return repeatWord;
    }

    public static String replaceExclamation(String text) {//Replace all exclamation points with a period
        String newString = text.replaceAll("!",".");
        return newString;
    }

    public static String shortenSpace(String text) {//Replaces double spaces with single spaces
        String newString = text.replaceAll("  ", " ");
        return newString;
    }

}