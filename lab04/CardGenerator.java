//lab04, 9/21/2018 
//Code by: Kenny Kwock/kwk222@lehigh.edu
/*
This program will generate a random number from 1-52, corresponding to a deck of cards. 
Assign random cards so you can practice some tricks
*/

import java.util.Random; //Importing a random number generator

public class CardGenerator{
  
  public static void main(String[] args){
    
    Random rand = new Random(); //Declaring my random number generator
    
    Integer n; //Declaring my integer n, which is the random number selected from 1-51
    
    n = rand.nextInt(52) + 1; //n is being assigned a random number
    
    String cardSuit = ""; //Integers and Strings are defined. String will be used for suit and identity
    String cardIdentity = "";
    Integer cardNumber;
    
    if (n>=1 && n<=13){ //Four if - else statements. These will determine the suit based on random number n
      cardSuit = "Diamonds";
    }
    else if (n>=14 && n<=26){
      cardSuit = "Clubs";
    }
    else if (n>=27 && n<=39){
      cardSuit = "Hearts";
    }
    else if (n>=40 && n<=52){
      cardSuit = "Spades";
    }
    
    cardNumber = n % 13; //Modulus, to determine the card identity
      
    switch (cardNumber) { //13 different cases, for 13 different types of cards
      case 0: cardIdentity = "King";
        break;
      case 1: cardIdentity = "Ace";
        break;
      case 2: cardIdentity = "2";
        break;
      case 3: cardIdentity = "3";
        break;
      case 4: cardIdentity = "4";
        break;
      case 5: cardIdentity = "5";
        break;
      case 6: cardIdentity = "6";
        break;
      case 7: cardIdentity = "7";
        break;
      case 8: cardIdentity = "8";
        break;
      case 9: cardIdentity = "9";
        break;
      case 10: cardIdentity = "10";
        break;
      case 11: cardIdentity = "Jack";
        break;
      case 12: cardIdentity = "Queen";
        break;      
    }
   
    System.out.println("Your card is the " + cardIdentity + " of " + cardSuit + "!"); //Print statement of random card
    
  }
  
}