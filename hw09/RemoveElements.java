// Kenny Kwock / kwk222@lehigh.edu
// Homework #9, 11/22/2018
// Homework 9.2 Array Practicing

import java.util.Scanner;
import java.util.Random;

public class RemoveElements {

    public static void main(String[] arg) {
        Scanner scan=new Scanner(System.in);
        int num[]=new int[10];
        int newArray1[];
        int newArray2[];
        int index, target;
        String answer="";
        do {
            System.out.print("Random input 10 ints [0-9]");
            num = randomInput(); //Array of 10 random integers
            String out = "The original array is:";
            out += listArray(num);
            System.out.println(out);

            System.out.print("Enter the index ");
            index = scan.nextInt();
            newArray1 = delete(num,index);
            String out1="The output array is ";
            out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"
            System.out.println(out1);

            System.out.print("Enter the target value ");
            target = scan.nextInt();
            newArray2 = remove(num,target);
            String out2="The output array is ";
            out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"
            System.out.println(out2);

            System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
            answer=scan.next();
        }while(answer.equals("Y") || answer.equals("y"));
    }

    public static String listArray(int num[]) {
        String out="{";
        for(int j=0;j<num.length;j++) {
            if(j>0) {
                out+=", ";
            }
            out+=num[j];
        }
        out+="} ";
        return out;
    }

    public static int[] randomInput() {//10 random integers
        Random rand = new Random();
        int[] randomArray = new int[10];
        boolean tempBool = false;
        for (int i = 0; i < 10; i ++) {
            randomArray[i] = rand.nextInt(10);
        }
        return randomArray;
    }

    public static int[] delete(int[] indexArray, int index) {
        int[] indexArrayNew = new int[9];//Since it is removing a single index, new array only has 9 elements
        int shift = 0;
        if ((index >= 0) && (index <= 9)) {
            for (int i = 0; i < 9; i++) {
                if (i == index) {
                    shift++;
                }
                if (shift == 0) {
                    indexArrayNew[i] = indexArray[i];
                }
                else {
                    indexArrayNew[i] = indexArray[i + 1];
                }
            }
            return indexArrayNew;
        }
        else {
            System.out.println("The index is not valid.");
            return indexArray;
        }
    }

    public static int[] remove(int[] equalArray, int target) {
        int counter = 0;
        for (int i = 0; i < equalArray.length; i++) {//Check number of occurrences, to make new array with a certain number of elements
            if (equalArray[i] == target) {
                counter++;
            }
        }

        int[] equalArrayNew = new int[equalArray.length - counter];

        counter = 0;
        skip: for (int j = 0; j < equalArray.length; j++) {//supre complicated code that removes all target numbers, and makes a new array
            if (equalArray[j] == target) {
                counter++;
                continue skip;
            }
            equalArrayNew[j - counter] = equalArray[j];
        }
        return equalArrayNew;
    }

}