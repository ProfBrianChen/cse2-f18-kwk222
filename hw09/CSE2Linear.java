// Kenny Kwock / kwk222@lehigh.edu
// Homework #9, 11/22/2018
// Homework 9.1 Array Practicing

import java.util.Scanner;
import java.util.Random;

public class CSE2Linear {

    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        Random rand = new Random();
        int[] finalGrade = new int[15];
        boolean tempBool = false;

        System.out.println("Please input score between 0 and 100 inclusive, hit ENTER for each submission, up to 15");
        System.out.println("Please input element 1: ");
        forloop:
        for (int i = 0; i < 15; i++) { //This whole loops is responsible for checking 3 key things, as you see below
            while (!tempBool) {
                tempBool = scnr.hasNextInt();
                if (tempBool == true) {//Primitvie Checker
                    finalGrade[i] = scnr.nextInt();
                    if ((finalGrade[i] >= 0) && (finalGrade[i] <= 100)) {
                        if (i == 0) {//implemnt i = 0 condition
                            tempBool = false;
                            System.out.println("Please input element " + (i + 2) + ": ");
                            continue forloop;
                        } else if (finalGrade[i] >= finalGrade[i - 1]) {
                            tempBool = false;
                            System.out.println("Please input element " + (i + 2) + ": ");
                            continue forloop;
                        } else {
                            tempBool = false;
                            System.out.println("Please input an integer greater or equal than the previous integer: ");
                        }
                    } else {//Within the Right Range
                        tempBool = false;
                        System.out.println("Please input an integer that is within range: ");
                    }
                } else {//Right Primitive
                    System.out.println("Please input the right primitive type: ");
                    scnr.next();
                }
            }
        }

        System.out.println("Here is the final output array: ");
        for (int j = 0; j < 15; j++) {
            System.out.print(finalGrade[j] + " ");
        }

        System.out.println();
        System.out.println("Please input exact grade to be searched for (Binary Search): ");
        int search = 0;
        tempBool = false;
        while (!tempBool) {//Another checker
            tempBool = scnr.hasNextInt();
            if (tempBool == true) {
                search = scnr.nextInt();
                if ((search >= 0) || (search <= 100)) {//refer to cse2 for flip
                    break;
                } else {
                    System.out.println("Please input an integer that is within range: ");
                    tempBool = false;
                }
            } else {
                System.out.println("Please input the right primitive type: ");
                scnr.next();
            }
        }

        int[] binaryCounter = binarySearch(finalGrade, search);//This is my way of returning 2 integers
        if (binaryCounter[0] == 1) {//One integer is to indicate of number was found, other integer is for iteration
            System.out.println(search + " was found in the list with " + binaryCounter[1] + " iteration(s). ");
        }
        else if (binaryCounter[0] == 0) {
            System.out.println(search + " was not found in the list with " + binaryCounter[1] + " iteration(s). ");
        }

        System.out.println();
        finalGrade = shuffling(finalGrade);
        System.out.println("Scrambled Array");
        for (int p = 0; p < 15; p++) {
            System.out.print(finalGrade[p] + " ");
        }

        System.out.println("Please input exact grade to be searched for (Linear Search): ");
        search = 0;
        tempBool = false;
        while (!tempBool) {//Another checker
            tempBool = scnr.hasNextInt();
            if (tempBool == true) {
                search = scnr.nextInt();
                if ((search >= 0) || (search <= 100)) {//refer to cse2 for flip
                    break;
                } else {
                    System.out.println("Please input an integer that is within range: ");
                    tempBool = false;
                }
            } else {
                System.out.println("Please input the right primitive type: ");
                scnr.next();
            }
        }

        int linearCounter = linearSearch(finalGrade, search);//Linear 
        if (linearCounter != -1) { //-1 means not found
            System.out.println(search + " was found in the list with " + linearCounter + " searches. ");
        }
        else {
            System.out.println("Value was not found at all with Linear Search. ");
        }
    }

    //This helper method was modified from Professor Carr's code, all credit goes to Professor Carr
    public static int[] binarySearch(int[] list, int key) {//two methods, # of iterations, and whether it is found or not
        int low = 0;
        int[] counter = {0, 0};//first value = whether it found or no, 0 no, 1 yes second value iteration
        int high = list.length - 1;
        while (high >= low) {
            int mid = (low + high) / 2;
            if (key < list[mid]) {
                high = mid - 1;
            }
            else if (key == list[mid]) {
                counter[0] = 1;
                return counter;
            }
            else {
                low = mid + 1;
            }
            counter[1]++;
        }
        return counter;
    }

    public static int linearSearch(int[] list, int key) {
        int counter = 0;
        for (int m = 0; m < 15; m++) {
            if (list[m] == key) {
                return counter;
            }
            else {
                counter++;
            }
        }
        counter = -1;
        return counter;
    }

    public static int[] shuffling(int[] arrayShuffle) {
        for (int k = 0; k < 1000; k++) {
            Random rand = new Random();
            int x = rand.nextInt(15);
            int tempValue = arrayShuffle[0];
            arrayShuffle[0] = arrayShuffle[x];
            arrayShuffle[x] = tempValue;
        }
        return arrayShuffle;
      }
  
}