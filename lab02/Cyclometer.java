//Lab 02, 9/7/2018 
//Code by: Kenny Kwock/kwk222@lehigh.edu
//This program calculates the distance and time of both trip 1 and trip 2, given how much time 
//and the number of rotations of the wheel both trips took

public class Cyclometer{
  // main method required for every program
  public static void main(String args[]){
    
  double secsTrip1=480.0;  //Time of trip 1
  double secsTrip2=3220.0;  //Time of trip 2
	int countsTrip1=1561;  //Number of rotations of trip 1
	int countsTrip2=9037; //Number of rotations of trip 2
    
  double wheelDiameter=27.0,  //The diameter of the wheel, crucial for calculating distance
  PI=3.14159, //Constant for calculating circumference
  feetPerMile=5280,  //Unit conversion
  inchesPerFoot=12,   //Unit conversionn
  secondsPerMinute=60;  //Unit conversion
	double distanceTrip1, distanceTrip2,totalDistance;  //Declaring 3 variables in which we'll store the answer in, then print out
    
    System.out.println("Trip 1 took " + secsTrip1/60 + " minutes and had " + countsTrip1 + " rotations");//Printing time
    System.out.println("Trip 2 took " + secsTrip2/60 + " minutes and had " + countsTrip2 + " rotations");
    
    distanceTrip1 = (wheelDiameter*PI*countsTrip1/inchesPerFoot)/feetPerMile;//Calculations
    distanceTrip2 = (wheelDiameter*PI*countsTrip2/inchesPerFoot)/feetPerMile;
    
    System.out.println("Trip 1 was " + distanceTrip1 + " miles long.");//Printing distance
    System.out.println("Trip 2 was " + distanceTrip2 + " miles long.");
    
    totalDistance = distanceTrip1 + distanceTrip2;//Calculations
    
    System.out.println("The total distance was " + totalDistance + " long.");//Printing total distance
    
  } // end of main method
  
} // end of the class