import java.util.Random;

public class arrayRandom {

    public static void main(String[] args) {
        Random rand = new Random(); //Declaring two arrays and random generator
        int[] randomArray = new int[100];
        int[] checker = new int[100];

        for (int i = 0; i < 100; i++) { //Assigns random values 0-99 into the randomArray[]
            randomArray[i] = rand.nextInt(100);
        }

        for (int j = 0; j < 100; j++) { //Examines each value. If equal, increment, if not, moves on
            for (int k = 0; k < 100; k++) {
                if (k == randomArray[j]) {
                    checker[k] = checker[k] + 1;
                }
            }
        }

        for (int x = 0; x < 100; x++) { //prints out the values. if there are 0 occurences, no prints
            if (checker[x] > 0) {
                System.out.println(x + " occurs " + checker[x] + " times.");
            }
        }
    }
}