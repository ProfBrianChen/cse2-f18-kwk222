// Kenny Kwock / kwk222@lehigh.edu
// Homework #5, 10/5/2018
// Program Purpose: Poker is a game of chance. In poker, players draw sets of five playing cards, called hands.
// Different hands have different probabilities of occurring.
// The objective of this homework is to use while loops to calculate these probabilities.
// We will limit our hands to those related to face values including:
// Four-of-a-kind, Three-of-a-kind, Two-Pair, and One-pair.


//loop an (numberofhands) amount of times, generating 5 distinct cards. For every probability this thing matches
// set individual counters and add 1 to them

//Draw each card individually, and then compare???

import java.util.Scanner; //Importing arrays, collections, and scanners
import java.util.Collections;
import java.util.ArrayList;


public class whileLoops {
    public static void main(String[] args) {

        Scanner myScanner = new Scanner(System.in);

        boolean tempBool; //Variables
        int numberOfHands = 0;
        double counterFour = 0;
        double counterThree = 0;
        double counterTwoPair = 0;
        double counterOnePair = 0;
        int counterHand = 0;
        double fourProbability = 0;
        double threeProbability = 0;
        double doublePairProbability = 0;
        double singlePairProbability = 0;

        do { //ensures users enter the right primitive for number of hands (integer)
            System.out.println("Please input the number of hands you would like generated (Integer Form): ");
            tempBool = myScanner.hasNextInt();
            if (tempBool){
                numberOfHands = myScanner.nextInt();
                break;
            }
            else {
                myScanner.next();
            }
        } while(!tempBool);

        while(counterHand < numberOfHands) {
            int card1 = 0; //Declaration and Serves to reset the card numbers every loop
            int card2 = 0;
            int card3 = 0;
            int card4 = 0;
            int card5 = 0;
          
          //Ok I think I could've used while loops, but that would've taken too long. If you have to deduct points because I 
          // used an array please do so

            ArrayList<Integer> cards = new ArrayList<Integer>();
            for (int i = 0; i < 52; i++) {
                cards.add(i + 1);
            }

            Collections.shuffle(cards);
            card1 = (int) cards.get(0);
            card2 = (int) cards.get(1);
            card3 = (int) cards.get(2);
            card4 = (int) cards.get(3);
            card5 = (int) cards.get(4);

            card1 = card1 % 13; // 0 = king, 1 = ace, ---, 12 = queen
            card2 = card2 % 13;
            card3 = card3 % 13;
            card4 = card4 % 13;
            card5 = card5 % 13;

            //Should act as a filter
          // These are all my conditions, I took into account all the possible cases, and used if statements to 
          // compare each card to each other

            if ( ((card1 == card2) && (card1 == card3) && (card1 == card4) && (card2 == card3) && (card2 == card4) && (card3 == card4)) ||
                    ((card1 == card2) && (card1 == card3) && (card1 == card5) && (card2 == card3) && (card2 == card5) && (card3 == card5)) ||
                    ((card1 == card2) && (card1 == card4) && (card1 == card5) && (card2 == card4) && (card2 == card5) && (card4 == card5)) ||
                    ((card1 == card3) && (card1 == card4) && (card1 == card5) && (card3 == card4) && (card3 == card5) && (card4 == card5)) ||
                    ((card2 == card3) && (card2 == card4) && (card2 == card5) && (card3 == card4) && (card3 == card5) && (card4 == card5)) ){
                counterFour = counterFour + 1;
            }
            else if ( ((card1 == card2) && (card2 == card3) && (card1 == card3) && (card1 != card4) && (card1 != card5) && (card2 != card4) && (card2 != card5) && (card3 != card4) && (card3 != card5)) ||
                    ((card1 == card2) && (card2 == card4) && (card1 == card4) && (card1 != card3) && (card1 != card5) && (card2 != card3) && (card2 != card5) && (card4 != card3) && (card4 != card5)) ||
                    ((card1 == card2) && (card2 == card5) && (card1 == card5) && (card1 != card3) && (card1 != card4) && (card2 != card3) && (card2 != card4) && (card5 != card3) && (card5 != card4)) ||
                    ((card1 == card3) && (card3 == card4) && (card1 == card4) && (card1 != card2) && (card1 != card5) && (card3 != card2) && (card3 != card5) && (card4 != card2) && (card4 != card5)) ||
                    ((card1 == card3) && (card3 == card5) && (card1 == card5) && (card1 != card2) && (card1 != card4) && (card3 != card2) && (card3 != card4) && (card5 != card2) && (card5 != card4)) ||
                    ((card1 == card4) && (card4 == card5) && (card1 == card5) && (card1 != card2) && (card1 != card3) && (card4 != card2) && (card4 != card3) && (card5 != card2) && (card5 != card3)) ||
                    ((card2 == card3) && (card3 == card4) && (card2 == card4) && (card2 != card1) && (card2 != card5) && (card3 != card1) && (card3 != card5) && (card4 != card1) && (card4 != card5)) ||
                    ((card2 == card3) && (card3 == card5) && (card2 == card5) && (card2 != card1) && (card2 != card4) && (card3 != card1) && (card3 != card4) && (card5 != card1) && (card5 != card4)) ||
                    ((card2 == card4) && (card4 == card5) && (card4 == card5) && (card2 != card1) && (card2 != card3) && (card4 != card1) && (card4 != card3) && (card5 != card1) && (card5 != card3)) ||
                    ((card3 == card4) && (card4 == card5) && (card3 == card5) && (card3 != card1) && (card3 != card2) && (card4 != card1) && (card4 != card2) && (card5 != card1) && (card5 != card2)) ){
                counterThree = counterThree + 1;
            }
            else if ( ((card1 == card2) && (card1 != card3) && (card1 != card4) && (card1 != card5) && (card2 != card3) && (card2 != card4) && (card2 != card5)) ||
                    ((card1 == card3) && (card1 != card2) && (card1 != card4) && (card1 != card5) && (card3 != card2) && (card3 != card4) && (card3 != card5)) ||
                    ((card1 == card4) && (card1 != card2) && (card1 != card3) && (card1 != card5) && (card4 != card2) && (card4 != card3) && (card4 != card5)) ||
                    ((card1 == card5) && (card1 != card2) && (card1 != card3) && (card1 != card4) && (card5 != card2) && (card5 != card3) && (card5 != card4)) ||
                    ((card2 == card3) && (card2 != card1) && (card2 != card4) && (card2 != card5) && (card3 != card1) && (card3 != card4) && (card3 != card5)) ||
                    ((card2 == card4) && (card2 != card1) && (card2 != card3) && (card2 != card5) && (card4 != card1) && (card4 != card3) && (card4 != card5)) ||
                    ((card2 == card5) && (card2 != card1) && (card2 != card3) && (card2 != card4) && (card5 != card1) && (card5 != card3) && (card5 != card4)) ||
                    ((card3 == card4) && (card3 != card1) && (card3 != card2) && (card3 != card5) && (card4 != card1) && (card4 != card2) && (card4 != card5)) ||
                    ((card3 == card5) && (card3 != card1) && (card3 != card2) && (card3 != card4) && (card5 != card1) && (card5 != card2) && (card5 != card4)) ||
                    ((card4 == card5) && (card4 != card1) && (card4 != card2) && (card4 != card3) && (card5 != card1) && (card5 != card2) && (card5 != card3)) ) {
                if ( ((((card1 == card2) && (card3 == card4)) || ((card1 == card3) && (card2 == card4)) || ((card1 == card4) && (card2 == card3))) && (card1 != card5) && (card2 != card5) && (card3 != card5) && (card4 != card5)) ||
                        ((((card1 == card2) && (card3 == card5)) || ((card1 == card3) && (card2 == card5)) || ((card1 == card5) && (card2 == card3))) && (card1 != card4) && (card2 != card4) && (card3 != card4) && (card5 != card4)) ||
                        ((((card1 == card2) && (card4 == card5)) || ((card1 == card4) && (card2 == card5)) || ((card1 == card5) && (card2 == card4))) && (card1 != card3) && (card2 != card3) && (card4 != card3) && (card5 != card3)) ||
                        ((((card1 == card3) && (card4 == card5)) || ((card1 == card4) && (card3 == card5)) || ((card1 == card5) && (card3 == card4))) && (card1 != card2) && (card3 != card2) && (card4 != card2) && (card5 != card2)) ||
                        ((((card2 == card3) && (card4 == card5)) || ((card2 == card4) && (card3 == card5)) || ((card2 == card5) && (card3 == card4))) && (card2 != card1) && (card3 != card1) && (card4 != card1) && (card5 != card1)) ){
                    counterTwoPair = counterTwoPair + 1;
                } else {
                    counterOnePair = counterOnePair + 1;
                }
            }
            else {
            }
                    counterHand = counterHand + 1;
        }

        fourProbability = counterFour / numberOfHands; //calculations for probability
        threeProbability = counterThree / numberOfHands;
        doublePairProbability = counterTwoPair / numberOfHands;
        singlePairProbability = counterOnePair / numberOfHands;

        System.out.println("Four of a kind: " + Math.round(fourProbability * 1000.0) / 1000.0); //rounded to 3 decimal places
        System.out.println("Three of a kind: " + Math.round(threeProbability * 1000.0) / 1000.0);
        System.out.println("Double Pair: " + Math.round(doublePairProbability * 1000.0) / 1000.0);
        System.out.println("Single Pair: " + Math.round(singlePairProbability * 1000.0) / 1000.0);
    }
}