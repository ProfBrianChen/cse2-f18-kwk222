//lab03, 9/14/2018 
//Code by: Kenny Kwock/kwk222@lehigh.edu
/*
This code will split the bill evenly among friends
Inputs include Original cost of check, % tip they are willing to pay, and how much each person needs to pay
*/

import java.util.Scanner; //Scanner 

public class Check{
  
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.print("Enter the original cost of the check, with 2 decimal places "); //Here are all of my inputs
    double checkCost = myScanner.nextDouble();
    
    System.out.print("Enter the percentage tip you would like to play, in the form of XX ");
    double tip = myScanner.nextDouble();
    
    System.out.print("Enter the number of people altogether, that went to the dinner ");
    int numPeople = myScanner.nextInt();
    
    double tipPercent; //declared tipPercent, which will be used to store a decimal number
    
    tipPercent = tip / 100.0;
    
    double totalCost; //Declaring my variables
    double costPerPerson;
    int dollars, dimes, pennies; 
    totalCost = checkCost * (1 + tipPercent);//Calculation for the totalCost
    costPerPerson = totalCost / numPeople;
    dollars=(int)costPerPerson;//The next 3 lines, are used to find the tenths and hundredths value in order to determine the exact price
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int)(costPerPerson * 100) % 10;
    
    System.out.println("Each person in the group owes approximately $" + dollars + "." + dimes + pennies); //final statement printed
      
  }
  
}
