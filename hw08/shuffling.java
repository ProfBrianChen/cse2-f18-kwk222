// Kenny Kwock / kwk222@lehigh.edu
// Homework #8, 11/11/2018
// This program will generate a deck of cards, shuffle it, and generate a hand of 5 (could be altered to generate a hand of 1-52 cards)

import java.util.Scanner;
import java.util.Random;

public class shuffling {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in); 
        String[] suitNames = {"C", "H", "S", "D"}; //suits club, heart, spade or diamond
        String[] rankNames = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
        String[] cards = new String[52];
        int numCards = 5;
        System.out.println("Number of cards in a hand: ");
      
        
        boolean tempBool = false;
          while (!tempBool) {
            tempBool = scan.hasNextInt();
            if (tempBool == true) {
                numCards = scan.nextInt();
                if ((numCards < 1) || (numCards > 52)) {
                    System.out.println("Please input an integer that is within range: ");
                    tempBool = false;
                }
                else {
                    break;
                }
            }
            else {
                System.out.println("Please input the right primitive type: ");
                scan.next();
            }
        }
      
        int again = 1;
        int index = 51;
        for (int i = 0; i < 52; i++) { //This is the initial creation of the card deck
            cards[i] = rankNames[i % 13] + suitNames[i / 13];
        }
        
        printArray(cards); //prints ordered card deck, shuffles, then prints out shuffled card deck
        System.out.println("SHUFFLED!");
        cards = shuffle(cards);
        printArray(cards);

        String[] hand = new String[numCards];

        while (again == 1) { //Loop that keeps on giving a new deck once the old deck runs out
            hand = getHand(cards, index, numCards);
            printArray(hand);
            index = index - numCards;
            if (index < (numCards - 1)) {
                System.out.println("Ran of cards, creating a new deck, please wait...");
                System.out.println();
                System.out.println();
                System.out.println("New Deck");
                for (int i = 0; i < 52; i++) {
                    cards[i] = rankNames[i % 13] + suitNames[i / 13];
                }
                printArray(cards);
                System.out.println("SHUFFLED!");
                cards = shuffle(cards);
                printArray(cards);
                index = 51;
            }
            System.out.println("Enter a 1 if you want another hand drawn");
            again = scan.nextInt();
        }
    }

    public static void printArray(String[] cards) { //Method for printing array of cards, and hands
        for (int i = 0; i < cards.length; i++) {
            System.out.print(cards[i] + " ");
        }
        System.out.println();
    }

    public static String[] shuffle(String[] cards) { //Method for shuffling the deck of cards
        Random rand = new Random();
        for (int i = 0; i < 100; i++) {
            int x = rand.nextInt(51) + 1;
            String tempString = cards[0];
            cards[0] = cards[x];
            cards[x] = tempString;
        }
        return cards;
    }

    public static String[] getHand(String[] cards, int index, int numCards) { //Method that assigns the last cards to hand
        String[] hand = new String[numCards];
        for (int j = 0; j < numCards; j++) {//might have to change these numbers
            hand[j] = cards[index - j];
        }
        return hand;
    }

}
