// Kenny Kwock / kwk222@lehigh.edu
// Homework #6, 10/18/2018
// This program will displaly an encrypted X using asterisks and spaces

import java.util.Scanner;

public class encryptedX {

    public static void main(String[] args) {

        Scanner myScanner = new Scanner(System.in);

        String symbol = "*"; //Declaring my symbol for pattern
        boolean tempBool; //tempbool for primitive + range checker
        int size = 0; //initializer

        do { //Checks primitive and range of number
            System.out.println("Please input the INTEGER of the desired pattern size (1-100 inclusive): ");
            tempBool = myScanner.hasNextInt();
            if (tempBool){
                size = myScanner.nextInt();
                if ((size < 1) || (size > 100)) { //Sets range of 3 to 100 (1 and 2 display nothing)
                    tempBool = false;
                }
                else {
                    break;
                }
            }
            else {
                myScanner.next();
            }
        } while (!tempBool);
        
        for (int i = 1; i <= size; i++) { //This for loop takes care of the columns
            for (int j = 1; j <= size; j++) { //This for loop takes cares of the rows
                if ((i == j) || (i == (size - j + 1))) { //These if else statements takes care of the spaces
                    System.out.print(" ");
                }
                else {
                    System.out.print(symbol);
                }
            }
            System.out.println();
        }
    }
}