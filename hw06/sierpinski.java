// Kenny Kwock / kwk222@lehigh.edu
// Homework #6, 10/18/2018
// This program will displaly all 5 generations with desired symbol

import java.util.Scanner;

public class sierpinski {
    public static void main(String[] args) {

        System.out.println("Please input the desired character for the Sierpinski Carpet: "); //Picks up first character
        Scanner myScanner = new Scanner(System.in);
        String temp = myScanner.next();
        char symbol = temp.charAt(0);

        //Generation 0
        System.out.println("Generation 0: ");
        System.out.println(symbol);
        System.out.println();

        //Generation 1
        System.out.println("Generation 1: ");
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                if(i == 1 && j == 1){
                    System.out.print(" ");
                }
                else{
                    System.out.print(symbol);
                }
            }
            System.out.println();
        }
        System.out.println();

        //Generation 2
        System.out.println("Generation 2: ");
        for(int i = 0; i < 9; i++) {
            for(int j = 0; j < 9; j++) {
                if((i/3 == 1 && j/3 == 1) || (i%3 == 1 && j%3 == 1)){
                    System.out.print(" ");
                }
                else{
                    System.out.print(symbol);
                }
            }
            System.out.println();
        }
        System.out.println();

        //Generation 3
        System.out.println("Generation 3: ");
        for(int i = 0; i < 27; i++) {
            for(int j = 0; j < 27; j++) {
                if((i/9 == 1 && j/9 == 1) || ((i/3)%3 == 1 && (j/3)%3 == 1) || (i%3 == 1 && j%3 == 1)){
                    System.out.print(" ");
                }
                else{
                    System.out.print(symbol);
                }
            }
            System.out.println();
        }
        System.out.println();

        //Generation 4
        System.out.println("Generation 4: ");
        for(int i = 0; i < 81; i++) {
            for (int j = 0; j < 81; j++) {
                if ((i/27 == 1 && j/27 == 1) || ((i/9)%3 == 1 && (j/9)%3 == 1) || ((i/3)%3 == 1 && (j/3)%3 == 1) || (i%3 == 1 && j%3 == 1)) {
                    System.out.print(" ");
                }
                else {
                    System.out.print(symbol);
                }
            }
            System.out.println();
        }
        System.out.println();

        //Generation 5
        //I THINK CODEANYWHERE CANT DISPLAY THE WHOLE GENERATION 5 CORRECTLY, PLEASE NO PENALIZE
        System.out.println("Generation 5: ");
        for(int i = 0; i < 243; i++) {
            for (int j = 0; j < 243; j++) {
                if ((i/81 == 1 && j/81 == 1) || ((i/27)%3 == 1 && (j/27)%3 == 1) || ((i/9)%3 == 1 && (j/9)%3 == 1) || ((i/3)%3 == 1 && (j/3)%3 == 1) || (i%3 == 1 && j%3 == 1)) {
                    System.out.print(" ");
                }
                else {
                    System.out.print(symbol);
                }
            }
            System.out.println();
        }
    }
}