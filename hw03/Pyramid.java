//hw03, 9/18/2018 
//Code by: Kenny Kwock/kwk222@lehigh.edu
/*
This code will calculate the volume of a pyramid, given information about the pyramid, such as base and height
*/

import java.util.Scanner; //Importing the Scanner 

public class Pyramid{
  
  public static void main(String[] args){
    
    double volume;//Declared the volume
    
    Scanner myScanner = new Scanner(System.in);//Declaring the scanner
    
    System.out.print("What is the square side of the pyramid (the base of the pyramid) ");//The next four lines are the input values
    double squareSide = myScanner.nextDouble();
    
    System.out.print("What is the height of the pyramid ");
    double height = myScanner.nextDouble();
    
    volume = Math.round((1.0 / 3.0) * squareSide * height);//calculations
    
    System.out.println("The volume of the pyramid is " + volume + " cubic units");//printed final volume based on inputs
     
  }
  
}

