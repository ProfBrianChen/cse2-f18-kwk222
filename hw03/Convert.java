//hw03, 9/18/2018 
//Code by: Kenny Kwock/kwk222@lehigh.edu
/*
This code will calculate the volume of water produced by the hurricane, using acres and inches of rainfall
*/

import java.util.Scanner; //Importing the Scanner 

public class Convert{
  
  public static void main(String[] args){
    
    double gallons, cubicMiles;//Declaring certain variables
      
    Scanner myScanner = new Scanner(System.in);//Declaring the scanner
    
    System.out.print("Enter the affected area in acres ");//next four lines are the input 
    double affectedAcres = myScanner.nextDouble();
    
    System.out.print("Enter the rainfall in the affected area in inches ");
    double affectedRainfall = myScanner.nextDouble();
    
    gallons = (affectedAcres * affectedRainfall * 27154.29);//calculations for gallons
    System.out.println("The number of gallons of rainfall is " + gallons);
    
    cubicMiles = (affectedAcres / 640) * (affectedRainfall * 1.57828e-5);//calculations for cubic miles
    System.out.println("The number of cubic miles of rainfall is " + cubicMiles);
    //ok for some reason, the calculation was super weird. I will calculate gallons and cubic miles kinda separately
    //I honestly hope yoou guys dont mind, I found all my calculations online
    
  }
  
}
