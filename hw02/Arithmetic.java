//hw02, 9/7/2018 
//Code by: Kenny Kwock/kwk222@lehigh.edu
/*
This code will calculate the total cost of items bought, including the 6% PA tax. 
*/

public class Arithmetic{
  
  public static void main(String args[]){
    
    int numPants = 3; //Here are the items declared
    int numShirts = 2;
    int numBelts = 1;
    
    double pantsPrice = 34.98; //Here are the price of the items declared
    double shirtPrice = 24.99;
    double beltCost = 33.99;
    
    double paSalesTax = 0.06; //Here is the PA sales tax, a constant of 6%
    
    double totalCostOfPants, totalCostOfShirts, totalCostOfBelts; //Total cost of each item
    double totalTaxOfPants, totalTaxOfShirts, totalTaxOfBelts; //Sales tax of buying all of each kind of item
    double totalCostWithoutTax; //Total cost of purchase
    double totalCostTaxAlone; //Total Sales Tax
    double totalCostAltogether; //Total paid for transaction, including cost
    
    totalCostOfPants = numPants * pantsPrice;
    totalCostOfShirts = numShirts * shirtPrice;
    totalCostOfBelts = numBelts * beltCost;
    
    totalTaxOfPants = numPants * pantsPrice * paSalesTax;
    totalTaxOfShirts = numShirts * shirtPrice * paSalesTax;
    totalTaxOfBelts = numBelts * beltCost * paSalesTax;
    
    totalCostWithoutTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    
    totalCostTaxAlone = totalTaxOfPants + totalTaxOfShirts + totalTaxOfBelts;
    
    totalCostAltogether = totalCostWithoutTax * (1+paSalesTax);
    
    /*For the calculation below, however much I tried, I could NOT convert the $6.3 into $6.30. I tried looking for online resoures, and none of that helped. 
    */
    System.out.println("The total cost of the pants, shirts, and belt, respectively are $" + totalCostOfPants + ", $" + totalCostOfShirts + ", and $" + totalCostOfBelts + ".");
    System.out.println("The total cost of the tax on pants, shirts, and belt, respectively are APPROXIMATELY $" + ((double) Math.round(totalTaxOfPants * 100.0)/100.0) + ", $" + ((double) Math.round(totalTaxOfShirts * 100.0)/100.0) + ", and $" + ((double) Math.round(totalTaxOfBelts * 100.0)/100.0) + ".");
    System.out.println("The total cost altogether, without tax,  of the pants, shirts, and belt is $" + totalCostWithoutTax + ".");
    System.out.println("The total cost altogether of the tax of the pants, shirts, and belt are APPROXIMATELY $" + ((double) Math.round(totalCostTaxAlone * 100.0)/100.0) + ".");
    System.out.println("The total cost altogether is APPROXIMATELY $" + ((double) Math.round(totalCostAltogether * 100.0)/100.0) + ".");
    
     
  }
  
}