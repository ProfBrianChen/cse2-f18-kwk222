public class WelcomeClass{
  
    public static void main(String args[]){
    
      System.out.println("   -----------   ");
      System.out.println("   | WELCOME |   ");
      System.out.println("   -----------   ");
      System.out.println("  ^  ^  ^  ^  ^  ^"); 
      System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
      System.out.println("<-k--w--k--2--2--2->"); 
      System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
      System.out.println("  v  v  v  v  v  v");
      
    }
  
}
/* For some odd reason, I can't use any slashes because I keep getting an illegal escape character error. 
if this homework assignment is incomplete, please let me know ASAP so I can fix it and turn it in ASAP 
without being penalized from an error that I have no clue how to fix at all */