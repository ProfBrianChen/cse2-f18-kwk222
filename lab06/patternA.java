//lab06, 10/12/2018 
//Code by: Kenny Kwock/kwk222@lehigh.edu
//This code will display patterns of values, based on input

import java.util.Scanner;

public class patternA {
    public static void main(String[] args) {

      
        Scanner myScanner = new Scanner(System.in); //Declaring variables
        boolean tempBool = false;
        int numberOfRows = 0;


        do { //Checks primitive and range of number
            System.out.println("Please input the INTEGER of the desired pattern size (Between 1-10, non-inclusive): ");
            tempBool = myScanner.hasNextInt();
            if (tempBool){
                numberOfRows = myScanner.nextInt();
                if ((numberOfRows < 1) || (numberOfRows > 10)) {
                    tempBool = false;
                }
                else {
                    break;
                }
            }
            else {
                myScanner.next();
            }
        } while (!tempBool);


        for (int i = 1; i <= numberOfRows; i++) { //First loop is for rows, second loop is for printing per row
            for (int j = 1; j <= i; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }

      
    }
}
