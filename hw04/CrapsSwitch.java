//hw04, 9/25/2018 
//Code by: Kenny Kwock/kwk222@lehigh.edu
/*
This program will play the game of craps, given two dice rolls, and determine the slang for it. 
This specified program will use SWITCH statements only. 
*/

import java.util.Scanner;//Importing the Scanner and the Random generator
import java.util.Random;

public class CrapsSwitch{
  
  public static void main(String[] args){
      
    Scanner myScanner = new Scanner(System.in);//Declaring my Scanner and Random
    Random rand = new Random();
    int inputMethod;//Declaring my variables
    int numberOne = 0;
    int numberTwo = 0;
    int absValue = 0;
    int sum = 0;
    boolean equals;
    String slang = "";
    
    System.out.println("Type 1 for manual input, and type 0 for random generation, of two dice rolls");
    inputMethod = myScanner.nextInt();
    
        /*The next set of switch statments generates 2 numbers, manual or automatic, based on whether the person
    picked manual or randomly generated. The final if statement serves to make sure the person knows how to read */
    
    
    switch (inputMethod){
      case 1: 
      System.out.println("You've chosen manual input, please input the two dice rolls separately (Must be between 1 - 6) ");
      System.out.println("Please input dice roll 1");
      numberOne = myScanner.nextInt();
      System.out.println("Please input dice roll 2");
      numberTwo = myScanner.nextInt();
        break;
        
      case 0:
      System.out.println("You've chosen random input, please wait while the two dice rolls are generating");
      numberOne = rand.nextInt(6) + 1;
      numberTwo = rand.nextInt(6) + 1;
        break;
        
      default:
      System.out.println("CONGRATS, YOU DON'T KNOW HOW TO READ");
      System.exit(0);
        break;        
    }
    
    /*The next set of TWO switch statements determines if the numbers the person entered is within range. If the 
    numbers are within range, nothing happens, but if the number isn't range, the person inputting the number gets a 
    custom error message and automatically exits the program. */
    
    switch (numberOne){
      case 1:
        break;
      case 2:
        break;
      case 3:
        break;
      case 4:
        break;
      case 5:
        break;
      case 6:
        break;
      default:
        System.out.println("CONGRATS, YOU DON'T KNOW HOW TO READ");
        System.exit(0);
        break; 
    }
    
    switch (numberTwo){
      case 1:
        break;
      case 2:
        break;
      case 3:
        break;
      case 4:
        break;
      case 5:
        break;
      case 6:
        break;
      default:
        System.out.println("CONGRATS, YOU DON'T KNOW HOW TO READ");
        System.exit(0);
        break; 
    }
    
    absValue = Math.abs(numberTwo - numberOne);//Declaring, for switch statement conditions
    sum = numberOne + numberTwo;
    
    /* A switch statment inside a switch statement. The first outer switch statmenet determines if the numbers are equal or not. 
    Based on whether they are equal or not, they follow separate paths to generate the slangs. 
    1+3 is different from 2+2 */
    
    switch (absValue){
      case 0: 
        switch (sum){
          case 4: slang = "Hard Four";
            break;
          case 6: slang = "Hard Six";
            break;
          case 8: slang = "Hard Eight";
            break;
          case 10: slang = "Hard Ten";
            break;
          case 12: slang = "Boxcars";
            break;
          default: slang = "0";
            break;  
        }
        break;
        
      default: 
        switch (sum){
          case 2: slang = "Snake Eyes";
            break;
          case 3: slang = "Ace Deuce";
            break;
          case 4: slang = "Easy Four";
            break;
          case 5: slang = "Fever Five";
            break;
          case 6: slang = "Easy Six";
            break;
          case 7: slang = "Seven out";
            break;
          case 8: slang = "Easy Eight";
            break;
          case 9: slang = "Nine";
            break;
          case 10: slang = "Easy Ten";
            break;
          case 11: slang = "Yo-leven";
            break;
          default: slang = "0";
            break;
        }
        break;
        }
    
    /* final switch statement serves to either print it out, or give an error message if the values didn't properly
    go through the previous switch statement */
    
    switch (slang){
      case "0": System.out.println("CONGRATS, YOU BROKE THE RULES AND TYPED SOME DICE ROLL OUT OF THE RANGE, EXCELLENT JOB");
        break;
      default:  System.out.println("You rolled a(n) " + slang + "!");
        break;
    }
    
    }
  }
  