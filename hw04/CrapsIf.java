//hw04, 9/25/2018 
//Code by: Kenny Kwock/kwk222@lehigh.edu
/*
This program will play the game of craps, given two dice rolls, and determine the slang for it. 
This specified program will use IF statements only. 
*/

import java.util.Scanner;//Importing the Scanner and the Random generator
import java.util.Random;

public class CrapsIf{
  
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);//Declaring my Scanner and Random
    Random rand = new Random();
    int inputMethod; //Declaring my variables
    int numberOne = 0;
    int numberTwo = 0;
    String slang = "";
    
    System.out.println("Type 1 for manual input, and type 0 for random generation, of two dice rolls");//Manual or Random
    inputMethod = myScanner.nextInt();
    
    /*The next set of 3 if statments generates 2 numbers, manual or automatic, based on whether the person
    picked manual or randomly generated. The final if statement serves to make sure the person knows how to read */
    
    if (inputMethod == 1){
      System.out.println("You've chosen manual input, please input the two dice rolls separately (Must be between 1 - 6) ");
      System.out.println("Please input dice roll 1");
      numberOne = myScanner.nextInt();
      System.out.println("Please input dice roll 2");
      numberTwo = myScanner.nextInt();
    }
    else if (inputMethod == 0){
      System.out.println("You've chosen random input, please wait while the two dice rolls are generating");
      numberOne = rand.nextInt(6) + 1;
      numberTwo = rand.nextInt(6) + 1;
    }
    else{
      System.out.println("CONGRATS, YOU DON'T KNOW HOW TO READ");
      System.exit(0);
    }
    
   /* the next if statement is used to determine if the numberOne and numberTwo are within range 
   if it isn't in the range, forcefully ends the program'*/
    
   if ((numberOne <= 0) || (numberOne >= 7)){
    System.out.println("CONGRATS, YOU DON'T KNOW HOW TO READ");
    System.exit(0);
   }
    
   if ((numberTwo <= 0) || (numberTwo >= 7)){
    System.out.println("CONGRAT, YOU DON'T KNOW HOW TO READ");
    System.exit(0);    
   }
    
    /* The next if statements determine what kind of roll it is with the slang, separated by 2 if statemnts, determines
    if numberOne is equal to numberTwo, and whether numberOne is not equal to numberTwo. 
    there is a difference between a 1 + 3 and a 2 + 2 */
    
   if (numberOne != numberTwo){
     if ((numberOne + numberTwo) == 2){
       slang = "Snake Eyes";
     }
     else if ((numberOne + numberTwo) == 3){
       slang = "Ace Deuce";
     }
     else if ((numberOne + numberTwo) == 4){
       slang = "Easy Four";
     }
     else if ((numberOne + numberTwo) == 5){
       slang = "Fever Five";
     }
     else if ((numberOne + numberTwo) == 6){
       slang = "Easy Six";
     }
     else if ((numberOne + numberTwo) == 7){
       slang = "Seven Out";
     }
     else if ((numberOne + numberTwo) == 8){
       slang = "Easy Eight";
     }
     else if ((numberOne + numberTwo) == 9){
       slang = "Nine";
     }
     else if ((numberOne + numberTwo) == 10){
       slang = "Easy Ten";
     }
     else if ((numberOne + numberTwo) == 11){
       slang = "Yo-leven";
     }
     else{
       slang = "0";
     }
   }
    else if (numberOne == numberTwo){
      if ((numberOne + numberTwo) == 4){
       slang = "Hard Four";
     }
      else if ((numberOne + numberTwo) == 6){
       slang = "Hard Six";
     }
      else if ((numberOne + numberTwo) == 8){
       slang = "Hard Eight";
     }
      else if ((numberOne + numberTwo) == 10){
       slang = "Hard Ten";
     }
      else if ((numberOne + numberTwo) == 12){
       slang = "Boxcars";
     }
      else{
       slang = "0";
      }
    }
    
    //Finally, this will either print the statement, or create an error if the rolls don't make sense
    
    if (slang == "0"){
      System.out.println("CONGRATS, YOU BROKE THE RULES AND TYPED SOME DICE ROLL OUT OF THE RANGE, EXCELLENT JOB");
    }
    else{
      System.out.println("You rolled a(n) " + slang + "!");
    }
    
  }
  
}