//lab05, 9/21/2018 
//Code by: Kenny Kwock/kwk222@lehigh.edu
/*
This program will ask for proper inputs about a certain class
If the inputs are the wrong type, will keep looping til it is correct
*/

import java.util.Scanner;

public class loops {
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    int courseNumber;
    int numberMeets;
    int hourHand;
    int minuteHand;
    int numberOfStudents;
    String departmentName = "";
    String instructorName = "";
    
    do {
      System.out.println("What is the course number of this class: ");
      courseNumber = myScanner.nextInt();
      if (myScanner.hasNextInt()) {
        break;
      }
      else {
        System.out.println("Please enter a valid course number: ");
      }
    } while (!myScanner.hasNextInt());
    
    
  }
}